import { memo, useState, useCallback, useMemo } from 'react';

const CountButton = memo(function CountButton({ callback, count, index }) {
	// Hạn chế log index ở đây
	console.log(`🆘 src/components/DualCounter.jsx`); // eslint-disable-line
	console.log(index); // eslint-disable-line
	console.log('%c => index ', 'background: #0095FF; color: #fff'); // eslint-disable-line
	const onClick = () => {
		// Hàm này k cần useCallback vì k bị re-render
		
		callback()
	}

  return <button onClick={onClick}>{count}</button>;
});

function DualCounter() {
  const [count1, setCount1] = useState(0);
  const [count2, setCount2] = useState(0);

	const memo1 = useMemo(() => count1, [count1]);
	const memo2 = useMemo(() => count2, [count2]);

	const memoIndex1 = useMemo(() => 1, []);
	const memoIndex2 = useMemo(() => 2, []);

	const onUpdate = useCallback((value, setValue) => () => {
		setValue(value)
	}, [])

	const onUpdateSpecify = useCallback((value, setValue) => {
		setValue(value)
	}, [])

  return (
    <>
      <CountButton count={memo1} callback={onUpdate(count1 + 1, setCount1)} index={memoIndex1} />
      <CountButton count={memo2} callback={() => onUpdateSpecify(count2 + 1, setCount2)} index={memoIndex2} />
    </>
  );
}

export default DualCounter;
